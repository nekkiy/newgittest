package ru.inordic.arj.gradle;

public class SuperOperand {
    private int op1;
    private int op2;

    public SuperOperand(int op1, int op2) {
        this.op1 = op1;
        this.op2 = op2;
    }

    public int add() {
        return op1 + op2;
    }

    public int sub() {
        return op1 - op2;
    }

    public int div() {
        return op1 / op2;
    }

    public void createAndThrowException(){
        throw new RuntimeException();
    }

}
