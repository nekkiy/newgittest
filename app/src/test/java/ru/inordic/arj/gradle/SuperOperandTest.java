package ru.inordic.arj.gradle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class SuperOperandTest {

    @Test
    void complexTest() throws Throwable {
        SuperOperand superOperand = new SuperOperand(1, 2);
        Assertions.assertEquals(3, superOperand.add());
        Assertions.assertEquals(-1, superOperand.sub());

        Assertions.assertEquals(0, superOperand.div());

        SuperOperand superOperand2 = new SuperOperand(1, 0);
        Executable callback = new Executable() {
            @Override
            public void execute(){
                superOperand2.createAndThrowException();
            }
        };
       Assertions.assertThrows(Exception.class,callback);

    }

}
